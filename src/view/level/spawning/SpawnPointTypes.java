package view.level.spawning;

/**
 * Lists all the possible spawning points.
 */
public enum SpawnPointTypes {

    /**
     * Player spawning point.
     */
    PLAYER,

    /**
     * The Knight spawning point.
     */
    KNIGHT,

    /**
     * The Archer spawning point.
     */
    ARCHER,

    /**
     * The thief spawning point.
     */
    THIEF,

    /**
     * Skeleton spawning point.
     */
    SKELETON,

    /**
     * Goblin spawning point.
     */
    GOBLIN,

    /**
     * Boss spawning point.
     */
    BOSS,

    /**
     * Red potion spawning point.
     */
    RED_POTION,

    /**
     * Blue potion spawning point.
     */
    BLUE_POTION
}
